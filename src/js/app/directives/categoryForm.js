'use strict';

angular.module('app').directive('categoryForm', function () {
    return {
        templateUrl: 'html/partials/category-form.html'
    }
});