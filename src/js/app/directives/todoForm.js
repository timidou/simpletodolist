'use strict';

angular.module('app').directive('todoForm', function () {
    return {
        templateUrl: 'html/partials/todo-form.html'
    }
});