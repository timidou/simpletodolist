'use strict';

angular.module('app').controller('mainController', function ($scope) {
    $scope.title = 'My ToDo List';

    $scope.importances = [{id: 0, name: 'hight'}, {id: 1, name: 'normal'}, {id: 2, name: 'low'}];
    $scope.categories = [{id: 0, name: 'home'}, {id: 1, name: 'work'}];

    $scope.todos = [];

    $scope.submitCategory = function (category) {
        $scope.categories.push({
            id: $scope.categories.length,
            name: category.name
        });
    };

    $scope.submitTodo = function (todo) {
        $scope.todos.push({
            idTodo: $scope.todos.length,
            idCategory: todo.category,
            importanceId: todo.importance,
            name: todo.name
        });
    };

    $scope.deleteTodo = function (item) {
        function removeTodoItem(value) {
            return value.idTodo != item.idTodo;
        }

        $scope.todos = $scope.todos.filter(removeTodoItem);
    }

});