module.exports = function (config) {
    config.set({
        basePath: '',

        frameworks: ['jasmine'],

        files: [
            'bower_components/angular/angular.js',
            'bower_components/angular-mocks/angular-mocks.js',
            'src/js/app/app.js',
            'src/js/app/directives/**/*.js',
            'src/js/app/controllers/**/*.js',
            'src/html/partials/*.html',
            'tests/jasmine/mainController.test.js'
        ],

        exclude: [],

        preprocessors: {
            'src/html/partials/*.html': ['ng-html2js']
        },

        ngHtml2JsPreprocessor: {
            stripPrefix: '',
            moduleName: 'html-partials'
        },

        reporters: ['progress'],

        port: 9876,

        colors: true,

        logLevel: config.LOG_INFO,

        autoWatch: false,

        browsers: ['PhantomJS'],

        singleRun: false
    })
}
