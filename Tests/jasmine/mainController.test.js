describe('mainController', function () {
    var ctrl, scope;

    beforeEach(module('app'));
    beforeEach(module('html-partials'));


    beforeEach(inject(function ($rootScope, $controller) {
        scope = $rootScope.$new();
        ctrl = $controller('mainController', {$scope: scope});
    }));

    it('should set a title property in the scope', function () {
        expect(scope.title).toBe('My ToDo List');
    });

    describe('submitCategory method', function () {

        it('should exists', function () {
            expect(scope.submitCategory).not.toBe(undefined);
        });

        it('should add an item to the category array', function () {
            scope.categories = [];

            scope.submitCategory({
                id: scope.categories.length,
                name: 'testFunction'
            });

            expect(scope.categories.length > 0).toBeTruthy();

        });

    });


    /*


     it('Unit Test function submitTodo', function () {
     scope.todos = [];

     scope.submitTodo({
     idTodo: scope.todos.length,
     idCategory: 1,
     importanceId: 1,
     name: 'testFunction'
     });

     expect(scope.todos.length > 0).toBeTruthy();
     });


     it('Unit Test function deleteTodo', function () {
     scope.todos = [];

     for (var i = 0; i < 2; i++) {
     scope.submitTodo({
     idTodo: scope.todos.length,
     idCategory: i,
     importanceId: i,
     name: 'testFunction'
     });
     }

     expect(scope.todos.length == 2).toBeTruthy();

     scope.deleteTodo({
     idTodo: 0,
     idCategory: 1,
     importanceId: 1,
     name: 'testFunction'
     });

     expect(scope.todos.length == 1).toBeTruthy();


     });

     */

});