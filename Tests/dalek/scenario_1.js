module.exports = {
    'Page title is correct': function (test) {
        test
            .open('http://localhost:63342/ToDoList/index.html')
            .assert.text('#title-site').is('My ToDo List')
            .done();
    },
    'Test to add a category': function (test) {
        test
            .open('http://localhost:63342/ToDoList/index.html/')
            .assert.text('#category-0').is('home')
            .done();
    }
};

