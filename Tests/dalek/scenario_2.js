module.exports = {
    'Test to add a category': function (test) {
        test
            .open('http://localhost:63342/ToDoList/index.html/')
            .type('#category', 'Add a category')
            .click('#input-category')
            .waitForElement('#category-2')
            .assert.text('#category-2').is('Add a category')
            .done();
    }
};