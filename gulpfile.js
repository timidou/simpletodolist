var gulp = require('gulp');
var Server = require('karma').Server;

var webserver = require('gulp-webserver');

gulp.task('webserver', function () {
    gulp.src(['src', 'bower_components'])
        .pipe(webserver({
            livereload: false,
            directoryListing: false,
            open: true
        }));
});

gulp.task('test', function (done) {
    new Server({
        configFile: __dirname + '/karma.conf.js',
        singleRun: true
    }, done).start();
});

gulp.task('dev', ['webserver']);
