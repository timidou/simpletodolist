# Todolist

## Prerequisites

- Git
- NodeJS

## Setup

- `git clone git@bitbucket.org:timidou/simpletodolist.git simpletodolist`
- `cd simpletodolist`
- `npm install`
- `bower install`

## How TO

### Start a development session

- `gulp dev`
- Enjoy : )

### run the unit tests

- `gulp test`
